const EC = require("elliptic")
import { Point } from "./point"
import { decimalToBinary, hexToDec, isPrime, mod } from "./number"

/**
 * Represent an elliptic curve
 */
export class Curve {
  //y² = x³ + ax + b
  curve

  constructor(a, b, p) {
    this.a = a
    this.b = b
    this.p = p
    this.curve = new EC.curve.short({
      type: "short",
      p: this.p,
      a: this.a,
      b: this.b
    })
  }

  /**
   * Get all point to create curve
   *
   * @param {number} limit
   * @param {number} step
   * @returns {Point[]}
   */
  getPoints(limit, step) {
    let pts = [],
      y = 0,
      i = Math.round(-limit)

    while (y < limit) {
      let y2 = Math.pow(i, 3) + this.a * i + this.b
      if (y2 > 0) {
        y = Math.sqrt(y2)

        pts.push(new Point(i, y))
        pts.unshift(new Point(i, -y))
      }
      i += step
    }
    return pts
  }

  /**
   * Return a list of point mod p
   *
   * @returns {Point|null}
   */
  getOriginPoint() {
    let r = this.p
    let a = this.a
    let b = this.b

    for (let i = 0; i < r; i++) {
      for (let j = 0; j <= r / 2; j++) {
        if ((j * j) % r === mod(i * i * i + a * i + b, r)) {
          return new Point(i, j)
        }
      }
    }
    return null
  }

  /**
   * Return a list of point mod p
   *
   * @returns {Point[]}
   */
  getAvaliablePoints() {
    let r = this.p
    let a = this.a
    let b = this.b

    let points = []
    for (let i = 0; i < r; i++) {
      for (let j = 0; j <= r / 2; j++) {
        if ((j * j) % r === mod(i * i * i + a * i + b, r)) {
          points.push(new Point(i, j))
          if (j % r !== (r - j) % r) {
            points.push(new Point(i, r - j))
          }
        }
      }
    }
    return points
  }

  groupOrder(point) {
    if (!isPrime(this.p)) {
      return 0
    }
    let n = 2
    let q = this.addSamePoint(point)
    while (q !== null) {
      q = this.addTwoPoint(point, q)
      n++
    }
    return n
  }

  /**
   * Get add of two point
   *
   * @param {Point} pta
   * @param {Point} ptb
   * @returns {Point|null}
   */
  addTwoPoint(pta, ptb) {
    let cPta = this.curve.point(pta.x, pta.y),
      cPtb = this.curve.point(ptb.x, ptb.y)
    let cptAdd = cPta.add(cPtb)
    if (cptAdd.isInfinity()) return null
    if (typeof pta !== EC.curve.base.BasePoint)
      return new Point(hexToDec(cptAdd.getX()), hexToDec(cptAdd.getY()))
    return cptAdd
  }
  /**
   * Get double of a point
   *
   * @param {Point} pt
   * @returns {Point|null}
   */
  addSamePoint(pt) {
    let cPt = this.curve.point(pt.x, pt.y)
    let cptDbl = cPt.dbl()
    if (cptDbl.isInfinity()) return null
    if (typeof pt !== EC.curve.base.BasePoint)
      return new Point(hexToDec(cptDbl.getX()), hexToDec(cptDbl.getY()))
    return cptDbl
  }

  /**
   * Get multiply of point
   *
   * @param {Point} pt
   * @param {number} num
   * @return {Point|null}
   */
  multPoint(pt, num) {
    let cPt = this.curve.point(pt.x, pt.y)
    let cptMult = cPt.mul(num)
    if (cptMult.isInfinity()) return null
    if (typeof pt !== EC.curve.base.BasePoint)
      return new Point(hexToDec(cptMult.getX()), hexToDec(cptMult.getY()))
    return cptMult
  }

  /**
   * Get double and add of point
   */
  applyDoubleAndAddMethod(point, k) {
    let pTemp = new Point(point.x, point.y)
    let kAsBinary = decimalToBinary(k) //0b1111111001
    let currentBit
    for (let i = 1; i < kAsBinary.length; i++) {
      currentBit = kAsBinary[i]
      // always apply doubling
      pTemp = this.addSamePoint(pTemp)
      if (currentBit == 1) {
        // add base point
        pTemp = this.addTwoPoint(pTemp, point)
      }
    }
    return pTemp
  }
}
