/* eslint-disable no-unused-vars */
import { Curve } from "./curve"
import { isPrime, modInverse } from "./number"
import { Point } from "./point"

/**
 * Get private key for point using brute force
 *
 * @param {Point} point
 * @param {Point} search
 * @param {number} a
 * @param {number} b
 * @param {number} mod
 */
export function bruteForce(point, search, a, b, mod) {
  let curve = new Curve(a, b, mod)
  let newPoint = curve.addSamePoint(point)
  let step = 2

  while (!newPoint.equal(search)) {
    newPoint = curve.addTwoPoint(newPoint, point)
    step++
  }
  return step
}

/**
 * Get private key for point using baby step giant step
 *
 * @param {Point} point
 * @param {Point} search
 * @param {number} a
 * @param {number} b
 * @param {number} mod
 */
export function bsgsElliptic(point, search, a, b, mod, order) {
  let curve = new Curve(a, b, mod)
  let m = Math.round(Math.sqrt(order)) + 1
  for (let i = 1; i < m; i++) {
    let iP = curve.applyDoubleAndAddMethod(point, i)
    for (let j = 1; j < m; j++) {
      let checkpoint = curve.applyDoubleAndAddMethod(point, j * m) //jm x P
      checkpoint = { x: checkpoint.x, y: mod - checkpoint.y }
      checkpoint = curve.addTwoPoint(search, checkpoint)
      if (iP.equal(checkpoint)) {
        return i + j * m
      }
    }
  }
  return -1
}
