import { BN } from "bn.js"

/**
 * Convert an hex to dec
 *
 * @param {BN|string} num
 */
export function hexToDec(num) {
  if (typeof num !== BN) {
    num = new BN(num, "hex")
  }
  return num.toNumber()
}

/**
 * Convert a dec to hex
 *
 * @param {BN|number} num
 */
export function decToHex(num) {
  if (typeof num !== BN) {
    num = new BN("" + num, 10)
  }
  return num.toString(16)
}

/**
 * Convert decimal number to binary
 *
 * @param {number} number
 * @returns {number[]}
 */
export function decimalToBinary(number) {
  let binary = []
  let temp = number
  while (temp > 0) {
    if (temp % 2 == 0) {
      binary.unshift(0)
    } else {
      binary.unshift(1)
    }
    temp = Math.floor(temp / 2)
  }
  return binary
}

/**
 * Test if value is prime
 *
 * @param {number} value
 * @returns {boolean}
 */
export function isPrime(value) {
  for (var i = 2; i < value; i++) {
    if (value % i === 0) {
      return false
    }
  }
  return value > 1
}

/**
 * Get mod of x
 *
 * @param {number} x
 * @param {number} r
 * @returns {number}
 */
export function mod(x, r) {
  return ((x % r) + r) % r
}

/**
 *
 * @param {number} a
 * @param {number} m
 */
export function modInverse(a, m) {
  // validate inputs
  ;[a, m] = [Number(a), Number(m)]
  if (Number.isNaN(a) || Number.isNaN(m)) {
    return NaN // invalid input
  }
  a = ((a % m) + m) % m
  if (!a || m < 2) {
    return NaN // invalid input
  }
  // find the gcd
  const s = []
  let b = m
  while (b) {
    ;[a, b] = [b, a % b]
    s.push({ a, b })
  }
  if (a !== 1) {
    return NaN // inverse does not exists
  }
  // find the inverse
  let x = 1
  let y = 0
  for (let i = s.length - 2; i >= 0; --i) {
    ;[x, y] = [y, x - y * Math.floor(s[i].a / s[i].b)]
  }
  return ((y % m) + m) % m
}
