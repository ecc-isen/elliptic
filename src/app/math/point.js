/**
 * Represent point with coordiantes
 */
export class Point {
  constructor(x, y) {
    this.x = x
    this.y = y
  }

  /**
   * Get distance between this and point in paramater
   *
   * @param {Point} pt
   */
  dist(pt) {
    return Math.sqrt(Math.pow(pt.x - this.x, 2) + Math.pow(pt.y - this.y, 2))
  }

  /**
   * Test if point is equal to point in parameter
   *
   * @param {Point} pt
   */
  equal(pt) {
    return this.x === pt.x && this.y === pt.y
  }

  /**
   * Get point for beizer curve
   *
   * @param {Point[]} pts
   * @param {number} tension
   * @param {number} numOfSegments
   */
  static getBezierPoints(pts, tension, numOfSegments) {
    // use input value if provided, or use a default value
    tension = typeof tension != "undefined" ? tension : 0.5
    numOfSegments = numOfSegments ? numOfSegments : 16

    let _pts = [],
      res = [], // clone array
      coords, // our x,y coords
      t1, // tension vectors
      t2,
      c1,
      c2,
      c3,
      c4, // cardinal points
      st,
      t,
      i // steps based on num. of segments

    // clone array so we don't change the original
    //
    _pts = pts.slice(0)

    // The algorithm require a previous and next point to the actual point array.
    // Check if we will draw closed or open curve.
    // If closed, copy end points to beginning and first points to end
    // If open, duplicate first points to befinning, end points to end

    _pts.unshift(pts[0]) //copy 1. point and insert at beginning
    _pts.push(pts[pts.length - 1]) //copy last point and append

    // ok, lets start..

    // 1. loop goes through point array
    // 2. loop goes through each segment between the 2 pts + 1e point before and after
    for (i = 1; i < _pts.length - 2; i++) {
      for (t = 0; t <= numOfSegments; t++) {
        // calc tension vectors
        // calc tension vectors
        coords = new Point(0, 0)
        t1 = new Point(0, 0)
        t2 = new Point(0, 0)

        t1.x = (_pts[i + 1].x - _pts[i - 1].x) * tension
        t2.x = (_pts[i + 2].x - _pts[i].x) * tension

        t1.y = (_pts[i + 1].y - _pts[i - 1].y) * tension
        t2.y = (_pts[i + 2].y - _pts[i].y) * tension

        // calc step
        st = t / numOfSegments

        // calc cardinals
        c1 = 2 * Math.pow(st, 3) - 3 * Math.pow(st, 2) + 1
        c2 = -(2 * Math.pow(st, 3)) + 3 * Math.pow(st, 2)
        c3 = Math.pow(st, 3) - 2 * Math.pow(st, 2) + st
        c4 = Math.pow(st, 3) - Math.pow(st, 2)

        // calc x and y cords with common control vectors
        coords.x = c1 * _pts[i].x + c2 * _pts[i + 1].x + c3 * t1.x + c4 * t2.x
        coords.y = c1 * _pts[i].y + c2 * _pts[i + 1].y + c3 * t1.y + c4 * t2.y

        //store points in array
        res.push(coords)
      }
    }
    return res
  }
}
