/**
 * function for creating an item using local storage
 * @param {string} localStorageName
 * @param {string|string[]|number|number[]} localStorageValue
 */
export function createItem(localStorageName, localStorageValue) {
  localStorage.setItem(localStorageName, localStorageValue)
}

/**
 * function to get item
 * @param {string} localStorageName
 * @returns {string}
 */
export function getItem(localStorageName) {
  return localStorage.getItem(localStorageName)
}

/**
 * function to delete item
 * @param {string} localStorageName
 */
export function deleteItem(localStorageName) {
  localStorage.removeItem(localStorageName)
}
/**
 * function to clear items
 */
export function clearItem() {
  localStorage.clear()
}

/**
 * function to get number of items
 *
 * @returns {number}
 */
export function getNumberItem() {
  return localStorage.length
}

/**
 * function to check if item already exist
 *
 * @param {string|JSON} item
 * @returns {string}
 */
export function itemExist(item) {
  for (let key in localStorage) {
    if (localStorage.getItem(key) === item) return key
  }
  return undefined
}

/**
 * function to get all key
 *
 * @returns {string[]}
 */
export function getKeys() {
  return Object.keys(localStorage)
}
