/**
 * @param {string} cookieName
 * @param {string} cookieValue
 * @param {int} days
 */
export function setCookie(cookieName, cookieValue, days) {
  if (days === undefined) {
    days = 365
  }
  let d = new Date()
  d.setTime(d.getTime() + 24 * 60 * 60 * 1000 * days)
  let expires = "expires=" + d.toUTCString()
  document.cookie = cookieName + "=" + cookieValue + ";" + expires + ";path=/"
}

/**
 *Function to get coockieName
 * @param {string} cookieName
 * @returns {string}
 */
export function getCookie(cookieName) {
  let ca = document.cookie.split(";")
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i].trim().split("=")
    if (cookieName == c[0]) {
      return c[1]
    }
  }
  return ""
}

/**
 * Funtion to delete cookie
 * @param {string} cookieName
 */
export function deleteCookie(cookieName) {
  setCookie(cookieName, "", -1)
}
