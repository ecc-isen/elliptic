/* eslint-disable no-unused-vars */
import p5 from "p5"
import { hexToDec } from "../math/number"
import { Point } from "../math/point"
import { getCookie, setCookie } from "../utilities/cookies"
import {
  createItem,
  deleteItem,
  getKeys,
  itemExist
} from "../utilities/localStorage"
import {
  GRID_AXIS_MAIN_COLOR,
  GRID_CURVE_COLOR,
  GRID_INIT_POINT_COLOR,
  GRID_SELECTED_POINT_COLOR
} from "./color"
import {
  Drawer,
  MAX_SEC,
  MAX_THI,
  MAX_ZOOM,
  MIN_SEC,
  MIN_THI,
  MIN_ZOOM
} from "./drawer"

export const GRID_ZOOM_COEFF = 2

/**
 * Main class to create grid and event for canvas
 */
export class Grid {
  p5Object = null
  canvasElement = ""
  canvasDiv
  cnv
  p5
  drawer

  curve
  width = 0
  height = 0
  c = new Point(0, 0)
  cr = new Point(0.3, 0.7)
  points
  showPoints

  constructor(canvasElement, curve) {
    this.canvasElement = canvasElement
    this.curve = curve
    this.points = this.curve.getAvaliablePoints()
    this.showPoint = getCookie("showPoint") === "true"
    this.p5Object = new p5(p5 => {
      this.p5 = p5
      this.drawer = new Drawer(this.p5)
      p5.setup = () => this.setupGrid()
      p5.draw = () => this.drawGrid()
      p5.mouseDragged = e => this.onMouseDragged(e)
      p5.windowResized = () => this.onWindowResized()
      p5.mouseWheel = e => this.onMouseWheel(e)
      p5.mouseReleased = () => this.onMouseReleased()
    })
  }

  /**
   * Setup canvas
   */
  setupGrid() {
    this.canvasDiv = document.getElementById(this.canvasElement)
    document.addEventListener("contextmenu", event => event.preventDefault())
    this.setWidthCanvas()
    this.cnv = this.p5.createCanvas(this.width, this.height)
    this.cnv.parent(this.canvasElement)
    this.setGridRelCenter(this.cr.x, this.cr.y)
  }

  /**
   * Draw grid
   */
  drawGrid() {
    this.p5.background(220)

    this.drawMultipleAxis(0.4, this.drawer.zoomThi)
    this.drawMultipleAxis(0.2, this.drawer.zoomSec)

    this.drawMainAxis()

    let limit =
      this.c.y < this.height / 2
        ? this.height + Math.abs(this.c.y)
        : Math.abs(this.c.y)
    let myPoints = this.curve.getPoints(
      limit / this.drawer.zoom,
      2 / this.drawer.zoom
    )
    this.drawCurve(myPoints, false, 16, true)
    if (this.showPoint) {
      this.drawPoint(this.points)
      this.drawPointText(this.points)
    }
  }

  /**
   * Draw main axis
   */
  drawMainAxis() {
    this.drawer.stroke(GRID_AXIS_MAIN_COLOR)
    this.drawer.setLineWidth(1.5)
    this.p5.line(this.c.x, 0, this.c.x, this.height)
    this.p5.line(0, this.c.y, this.width, this.c.y)
  }

  /**
   * Set real center
   *
   * @param {number} x
   * @param {number} y
   */
  setGridCenter(x, y) {
    this.c = new Point(x, y)
    this.drawer.setOffset(x, y)
    this.cr = new Point(this.c.x / this.width, this.c.y / this.height)
  }

  /**
   * Set relative center
   *
   * @param {number} rx
   * @param {number} ry
   */
  setGridRelCenter(rx, ry) {
    this.cr = new Point(rx, ry)
    this.c = new Point(this.cr.x * this.width, this.cr.y * this.height)
    this.drawer.setOffset(this.c.x, this.c.y)
  }

  /**
   * Draw outline canvas
   */
  drawOutline() {
    this.drawer.p5.rect(0, 0, this.width, this.height)
  }

  /**
   * Draw grid pattern
   *
   * @param {number} width
   * @param {number} space
   */
  drawMultipleAxis(width, space) {
    this.drawer.setLineWidth(width)
    this.drawer.stroke(GRID_AXIS_MAIN_COLOR)
    let xOffset =
      this.drawer.offset.x > 0
        ? Math.abs(this.drawer.offset.x) % space
        : space - (Math.abs(this.drawer.offset.x) % space)

    let yOffset =
      this.drawer.offset.y > 0
        ? Math.abs(this.drawer.offset.y) % space
        : space - (Math.abs(this.drawer.offset.y) % space)

    do {
      this.drawer.p5.line(xOffset, 0, xOffset, this.height)
      this.drawer.p5.line(0, yOffset, this.width, yOffset)

      xOffset += space
      yOffset += space
    } while (xOffset < this.width || yOffset < this.height)
  }

  /**
   * Draw curve with points
   *
   * @param {Point[]} pts
   */
  drawCurve(pts) {
    this.drawer.stroke(GRID_CURVE_COLOR)

    this.drawLinesPoint(pts)
  }

  /**
   * Draw list of point
   *
   * @param {GraphPoint[]} pts
   */
  drawPoint(pts) {
    for (let i = 0; i < pts.length; i++) {
      if (itemExist(JSON.stringify(pts[i]))) {
        this.drawer.stroke(GRID_SELECTED_POINT_COLOR)
      } else {
        this.drawer.stroke(GRID_INIT_POINT_COLOR)
      }
      this.drawer.drawRect(
        pts[i].x * this.drawer.zoom - 2,
        pts[i].y * this.drawer.zoom - 2,
        4 - this.drawer.offset.x,
        4 + this.drawer.offset.y
      )
    }
  }

  /**
   * Draw list of point
   *
   * @param {GraphPoint[]} pts
   */
  drawPointText(pts) {
    for (let i = 0; i < pts.length; i++) {
      if (itemExist(JSON.stringify(pts[i]))) {
        this.drawer.stroke(GRID_SELECTED_POINT_COLOR)
        this.drawer.drawText(
          "Point : (" + pts[i].x + ", " + pts[i].y + ")",
          pts[i].x * this.drawer.zoom,
          pts[i].y * this.drawer.zoom + 16
        )
      } else {
        this.drawer.stroke(GRID_INIT_POINT_COLOR)
      }
    }
  }

  /**
   * Draw lines between point
   *
   * @param {Point[]} pts
   */
  drawLinesPoint(pts) {
    for (let i = 0; i < pts.length - 1; i++) {
      this.drawer.drawLine(
        pts[i].x * this.drawer.zoom,
        pts[i].y * this.drawer.zoom,
        pts[i + 1].x * this.drawer.zoom,
        pts[i + 1].y * this.drawer.zoom
      )
    }
  }

  drawAnimationBetweenPoints(pts) {}

  /**
   * Set size of the canvas with window size
   */
  setWidthCanvas() {
    if (this.canvasDiv === undefined) {
      this.width = this.p5.windowWidth / 2
      this.height = this.p5.windowHeight / 2
    } else {
      this.width = this.canvasDiv.offsetWidth
      this.height = this.canvasDiv.offsetHeight
    }
  }

  /**
   * Check if mouse on canvas
   *
   * @returns {boolean}
   */
  isOnCanvas() {
    return (
      this.p5.mouseX < this.width &&
      this.p5.mouseX > 0 &&
      this.p5.mouseY < this.height &&
      this.p5.mouseY > 0
    )
  }

  /**
   * Get available point number
   *
   * @param {string[]} keys
   * @returns {number}
   */
  getAvailablePointNumber(keys) {
    let available = 1
    keys.forEach(elem => {
      if (elem.match("P:[0-9]+") !== null) {
        if (elem.split(":")[1] != available) return
        available++
      }
    })
    return available
  }

  // Events

  /**
   * Event when mouse is dragged
   */
  onMouseDragged() {
    if (this.isOnCanvas())
      if (this.p5.mouseButton === this.p5.LEFT) {
        let offsetX = this.p5.mouseX - this.p5.pmouseX
        let offsetY = this.p5.mouseY - this.p5.pmouseY
        this.setGridCenter(this.c.x + offsetX, this.c.y + offsetY)
      }
    return false
  }

  /**
   * Event when window is resized
   */
  onWindowResized() {
    this.setWidthCanvas()
    this.setGridRelCenter(this.cr.x, this.cr.y)
    this.p5.resizeCanvas(this.width, this.height)
  }

  /**
   * Event when choose zoom in
   */
  onZoomIn() {
    if (this.drawer.zoom < MAX_ZOOM) {
      this.drawer.zoom *= GRID_ZOOM_COEFF

      if (this.drawer.zoomThi < MAX_THI) {
        this.drawer.zoomThi *= GRID_ZOOM_COEFF
      } else {
        this.drawer.zoomThi = MIN_THI
      }

      if (this.drawer.zoomSec < MAX_SEC) {
        this.drawer.zoomSec *= GRID_ZOOM_COEFF
      } else {
        this.drawer.zoomSec = MIN_SEC
      }

      this.p5.redraw()
    }
  }

  /**
   * Event when choose zoom out
   */
  onZoomOut() {
    if (this.drawer.zoom > MIN_ZOOM) {
      this.drawer.zoom /= GRID_ZOOM_COEFF

      if (this.drawer.zoomThi > MIN_THI) {
        this.drawer.zoomThi /= GRID_ZOOM_COEFF
      } else {
        this.drawer.zoomThi = MAX_THI
      }

      if (this.drawer.zoomSec > MIN_SEC) {
        this.drawer.zoomSec /= GRID_ZOOM_COEFF
      } else {
        this.drawer.zoomSec = MAX_SEC
      }

      this.p5.redraw()
    }
  }

  /**
   * Event whene choose reset grid
   */
  onResetGrid() {
    this.drawer.zoom = 32
    this.drawer.zoomThi = MAX_THI / 2
    this.drawer.zoomSec = MAX_SEC / 2

    this.setGridRelCenter(0.5, 0.5)

    this.p5.redraw()
  }

  /**
   * Event when mouse wheel
   */
  onMouseWheel(event) {
    if (this.isOnCanvas()) {
      let scrollDelta = event.delta
      if (scrollDelta < 0) {
        this.onZoomIn()
      } else {
        this.onZoomOut()
      }
    }
  }

  /**
   * Event when mouse clicked
   */
  onMouseReleased() {
    if (this.p5.mouseButton === this.p5.RIGHT) {
      let mousePt = new Point(this.p5.mouseX, this.p5.mouseY)
      let offset = this.drawer.offset
      let zoom = this.drawer.zoom
      let size = 5
      let pt = this.points.find(element => {
        return (
          offset.x + element.x * zoom > mousePt.x - size &&
          offset.x + element.x * zoom < mousePt.x + size &&
          offset.y - element.y * zoom > mousePt.y - size &&
          offset.y - element.y * zoom < mousePt.y + size
        )
      })
      if (pt !== undefined) {
        let json = JSON.stringify(pt)
        let key
        if ((key = itemExist(json)) !== undefined) {
          deleteItem(key)
        } else {
          createItem(
            "P:" + this.getAvailablePointNumber(getKeys().sort()),
            json
          )
        }
        this.p5.redraw()
      }
    }
  }

  /**
   * Event to show mod point
   */
  onShowPoints() {
    this.showPoint = !this.showPoint
    setCookie("showPoint", this.showPoint)
  }
}
