export const GRID_BACK_COLOR = "rgb(235, 235, 235)"
export const GRID_AXIS_MAIN_COLOR = "rgb(40, 40, 40)"
export const GRID_CURVE_COLOR = "rgb(255, 0, 0)"

export const GRID_INIT_POINT_COLOR = "rgb(255, 0, 0)"
export const GRID_SELECTED_POINT_COLOR = "rgb(0, 0, 255)"
