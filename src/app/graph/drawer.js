import { Point } from "../math/point"

export const FONT_SIZE = 10
export const MIN_ZOOM = 2 ^ -8
export const MAX_ZOOM = 512
export const MIN_THI = 40
export const MIN_SEC = 10
export const MAX_THI = 160
export const MAX_SEC = 40

/**
 * Class use to draw element on canvas
 */
export class Drawer {
  p5

  zoom = 16
  zoomThi = MAX_THI / 2
  zoomSec = MAX_SEC / 2
  offset = new Point(0, 0)

  constructor(p5) {
    this.p5 = p5
  }

  /**
   * Draw a line between two points
   *
   * @param {number} x1
   * @param {number} y1
   * @param {number} x2
   * @param {number} y2
   */
  drawLine(x1, y1, x2, y2) {
    this.p5.line(
      this.offset.x + x1,
      this.offset.y - y1,
      this.offset.x + x2,
      this.offset.y - y2
    )
  }

  /**
   * Draw a rect between two points
   *
   * @param {number} x1
   * @param {number} y1
   * @param {number} x2
   * @param {number} y2
   */
  drawRect(x1, y1, x2, y2) {
    this.p5.rect(
      this.offset.x + x1,
      this.offset.y - y1,
      this.offset.x + x2,
      this.offset.y - y2
    )
  }

  /**
   * Draw text with coordinates
   *
   * @param {string} text
   * @param {number} x
   * @param {number} y
   */
  drawText(text, x, y) {
    this.p5.textSize(FONT_SIZE)
    this.p5.textAlign(this.p5.CENTER, this.p5.CENTER)
    this.p5.rect(
      this.offset.x + x - 3 * text.length,
      this.offset.y - y - FONT_SIZE,
      3 * text.length * 2,
      16
    )
    this.p5.fill(0)
    this.p5.text(text, this.offset.x + x, this.offset.y - y)
    this.p5.fill(255)
  }

  /**
   * Set offset point
   *
   * @param {number} x
   * @param {number} y
   */
  setOffset(x, y) {
    this.offset.x = x
    this.offset.y = y
  }

  /**
   * Set line width
   *
   * @param {number} width
   */
  setLineWidth(width) {
    this.p5.strokeWeight(width)
  }

  /**
   * Set color on draw
   *
   * @param {Color} color
   */
  stroke(color) {
    if (color !== undefined) this.p5.stroke(color)
    else this.p5.stroke()
  }

  /**
   * Get position relative of the canvas
   *
   * @param {number} mouseX
   * @param {number} mouseY
   * @returns {Point}
   */
  getRelativePos(mouseX, mouseY) {
    return new Point(
      (mouseX - this.offset.x) / this.zoom,
      (this.offset.y - mouseY) / this.zoom
    )
  }

  /**
   * Set background color
   *
   * @param {Color} color
   */
  setBackground(color) {
    this.p5.background(color)
  }
}
