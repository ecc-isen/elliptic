/**
 *
 */

// var userLang = navigator.language || navigator.userLanguage
// alert("The language is: " + userLang)

export const about = {
  title: "A propos",
  titlePart1: "Introduction au projet d'explorateur elliptique :",
  contentPart1: [
    `La cryptographie asymétrique (à clé publique) est basée sur l'existence de certains problèmes
    calculatoires difficiles. Si les premières méthodes proposées se basaient sur la difficulté de factoriser de 
    grands entiers ou d'autres problèmes arithmétiques, les chercheurs ont rapidement réalisé qu'il serait
    intéressant pour les applications de considérer l'analogue de ces problèmes pour des objets a priori plus
    compliqués appelés courbes elliptiques.`,
    `Ce sont des objets géométriques sur lesquels on peut définir une loi d'addition « tordue » permettant
    d'énoncer plus économiquement qu'avec les entiers usuels des problèmes difficiles à résoudre pour la
    cryptographie. Si ces courbes possèdent des définitions précises permettant d'effectuer explicitement (et
    efficacement) les calculs nécessaires, elles peuvent rester mystérieuses (voire intimidantes) pour le
    néophyte.`,
    `Le but de ce projet est d’implémenter une application web permettant d’effectuer et visualiser des
    calculs sur une courbe elliptique. Un prototype encapsulant les fonctionnalités de base du logiciel de
    calcul mathématique Sagemath a été réalisée l’année dernière et devra être portée en JavaScript avec 
    l'utilisation du framework Vue.js d avantde pouvoir envisager l’implémentation d’algorithmes plus sophistiqués 
    (dénombrement, logarithme discret, ...)`
  ],

  titlePart2: "Les courbes elliptiques",
  contentPart2: [
    `En mathématiques, une courbe elliptique est un cas particulier de courbe algébrique, munie entre 
  autres propriétés d'une addition géométrique sur ses points.`,
    `Les courbes elliptiques ont de nombreuses applications dans des domaines très différents des mathématiques : elles 
  interviennent ainsi en mécanique classique dans la description du mouvement des toupies, en théorie des nombres dans
   la démonstration du dernier théorème de Fermat, en cryptologie dans le problème de la factorisation des entiers ou 
   pour fabriquer des codes performants. `,
    `Dans notre explorateur, les courbes seront définies par l'équation y^2 = x^3 + a*x + b, ou a et b sont des eniers.`,

    `Tous les points de la courbe de coordonnées réelles vérifient l’équation ainsi qu’un point à l’infini. Ce point à 
   l’infini est utilisé comme élément neutre pour effectuer les additions de points. On peut l’imaginer comme l’intersection
   de toutes les droites verticales. Dans une utilisation pratique, on le fixera à l’infini (CF schéma ci-dessous).`,
    `Propriétés :`
  ],

  //TODO placer l'image
  listeproriete: [
    `
       xp≠xq
       et 
       yp≠yq
       : nous traçons une droite passant par ces deux points, et prenons l’opposé du point d’intersection de cette droite avec la courbe.
      Mathématiquement cela correspond à :`,
    `xp+q=s2−xp−xq`,
    `yp+q=−yr=−s(s2−xp−xq)−t`,
    `Ou`,
    `xp, xq,yp,yqv les coordonnées des deux points d’origine, et : `,
    `y=sx+t`,
    `la droite d’intersection des deux points définie par les équations:`,
    ` s=(yp−yq)xp−xq`,
    `t=(yqxp−ypxq)xp−xq`,

    `xp=xq et yp≠yq ; la droite joignant les deux points est 
    verticale, le point P+Q est donc le point à l’infini `,

    `xp=xq et yp=yq ≠0 `,
    `Ici P = Q et le point n’est pas sur l'axe des abscisses.`,
    `y=sx+t est donc la tangente à la courbe au point p avec:`,
    `s=(3x3p+a)2yp`,
    `t= yp− (3x3p+a)xp/2yp`,
    `x2p= s2−2xp`,
    `y2p= − yp+s(xp−x2p)`,
    `xp=xq et yp=yq =0`,
    `le point est donc sur l’axe des abscisses, la tangente en ce point est verticale,
    le point est d’intersection est donc à l’infini.`,
    `On a donc P + P = 2P à l’infini.`
  ], //a mettre en Orderedlist
  titlePart3: "Le logarithme discret",
  contentPart3: [
    `Soit G un groupe fini cyclique et g l’un de ses générateurs, alors pour tout 
  élément y de G, il existe un entier positif x tel que y=g^x.`,
    `Le plus petit de ces entiers est appelé l’index, ou le logarithme discret, de y en base g. On l'appelle discret car
  ici on ne travaille qu'avec des entiers.`,
    `La loi reliant les éléments les nombres relèves de calculs mathématiques complexes.
     En effet, la loi mathématique définissant le groupe de nombres étant définie de manière privée, cela nécessite, pour un attaquant
     n’ayant pas cette information, un nombre de calculs complexe très élevé. C’est le cas pour le chiffrement RSA, mais également pour
     la cryptographie par courbe elliptique.`
  ]
}
