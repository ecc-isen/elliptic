import { expect, test } from "@jest/globals"
import { Curve } from "../../../src/app/math/curve"
import { isPrime } from "../../../src/app/math/number"
import { Point } from "../../../src/app/math/point"

var EC = require("elliptic")

var a = 3
var b = 17
var p = 10007
var x = 56
var y = 759

test("Test add two point", () => {
  let curve = new Curve(a, b, p)
  var t0 = performance.now()

  let point = curve.addTwoPoint(new Point(x, y), new Point(43, 98))

  var t1 = performance.now()
  console.log("Call to addTwoPoint took " + (t1 - t0) + " milliseconds.")
  expect(point.x).toEqual(4618)
  expect(point.y).toEqual(3600)
})

test("Test add same point", () => {
  let curve = new Curve(a, b, p)
  var t0 = performance.now()

  let point = curve.addSamePoint(new Point(x, y))

  var t1 = performance.now()
  console.log("Call to addSamePoint took " + (t1 - t0) + " milliseconds.")
  expect(point.x).toEqual(1538)
  expect(point.y).toEqual(1405)

  curve = EC.curves.secp256k1.curve
  let p1 = curve.g
  t0 = performance.now()
  point = p1.add(p1)
  t1 = performance.now()
  console.log("Call to addSamePoint lib took " + (t1 - t0) + " milliseconds.")
  expect(JSON.stringify(point.x)).toEqual(
    JSON.stringify(
      "c6047f9441ed7d6d3045406e95c07cd85c778e4b8cef3ca7abac09b95c709ee5"
    )
  )
  expect(JSON.stringify(point.y)).toEqual(
    JSON.stringify(
      "1ae168fea63dc339a3c58419466ceaeef7f632653266d0e1236431a950cfe52a"
    )
  )
})

test("Test mult point", () => {
  let curve = new Curve(a, b, p)
  var t0 = performance.now()
  let point = curve.multPoint(new Point(x, y), 6512)

  var t1 = performance.now()
  console.log("Call to multPoint took " + (t1 - t0) + " milliseconds.")
  expect(point.x).toEqual(6561)
  expect(point.y).toEqual(8826)

  curve = EC.curves.secp256k1.curve
  let p1 = curve.g

  t0 = performance.now()
  point = p1.mul(6512)
  t1 = performance.now()
  console.log("Call to multPoint lib took " + (t1 - t0) + " milliseconds.")
  expect(JSON.stringify(point.x)).toEqual(
    JSON.stringify(
      "cfdf4bb856c3bbce3d639e960b4eba5af865af3129eb97a46c52328769087e13"
    )
  )
  expect(JSON.stringify(point.y)).toEqual(
    JSON.stringify(
      "abd7e01b44de8f1d075eb090effd5a0f51bf87bfb76491ebe8e168ddeeff8c99"
    )
  )
})

test("Test double and add", () => {
  let curve = new Curve(a, b, p)
  var t0 = performance.now()
  let point = curve.applyDoubleAndAddMethod(new Point(x, y), 3)

  var t1 = performance.now()
  console.log(
    "Call to applyDoubleAndAddMethod took " + (t1 - t0) + " milliseconds."
  )

  t0 = performance.now()
  let test = curve.addSamePoint(new Point(x, y))
  test = curve.addTwoPoint(test, new Point(x, y))

  t1 = performance.now()
  console.log(
    "Call to addSamePoint and addTwoPoint took " + (t1 - t0) + " milliseconds."
  )

  expect(point.x).toEqual(test.x)
  expect(point.y).toEqual(test.y)
})

test("Test order group point", () => {
  let curve = new Curve(a, b, p)
  let point = new Point(x, y)
  var t0 = performance.now()

  let order = curve.groupOrder(point)

  var t1 = performance.now()
  console.log("Call to groupOrder took " + (t1 - t0) + " milliseconds.")
  expect(order).toEqual(9871)
  expect(isPrime(order)).not.toBeFalsy()
})
