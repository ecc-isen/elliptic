import { expect, test } from "@jest/globals"
import {
  decimalToBinary,
  decToHex,
  hexToDec
} from "../../../src/app/math/number"

test("Test dec to hex", () => {
  expect(decToHex(10)).toEqual("a")
  expect(decToHex(-10)).toEqual("-a")
})

test("Test dec to hex", () => {
  expect(hexToDec("a")).toEqual(10)
  expect(hexToDec("-a")).toEqual(-10)
})

test("Test dec to bin", () => {
  let bin = decimalToBinary(1)
  expect(bin.length).toEqual(1)
  expect(bin[0]).toEqual(1)

  bin = decimalToBinary(10)
  expect(bin.length).toEqual(4)
  expect(bin[0]).toEqual(1)
  expect(bin[1]).toEqual(0)
  expect(bin[2]).toEqual(1)
  expect(bin[3]).toEqual(0)
})
