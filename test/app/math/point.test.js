import { expect, test } from "@jest/globals"
import { Point } from "../../../src/app/math/point"

test("Test create default point", () => {
  let pt = new Point(0, 0)

  expect(pt.x).toEqual(0)
  expect(pt.y).toEqual(0)
})

test("Test create point", () => {
  let pt = new Point(10, 10)

  expect(pt.x).toEqual(10)
  expect(pt.y).toEqual(10)
})

test("Test distance between points", () => {
  let pta = new Point(0, 0)
  let ptb = new Point(0, 10)
  let ptc = new Point(10, 0)

  expect(pta.dist(ptb)).toEqual(10)
  expect(pta.dist(ptc)).toEqual(10)
})
