import { expect, test } from "@jest/globals"
import { Curve } from "../../../src/app/math/curve"
import { bruteForce, bsgsElliptic } from "../../../src/app/math/find"
import { Point } from "../../../src/app/math/point"

var a = 3
var b = 17
var p = 10007
var p2 = 100003
var n = 6515
var n2 = 46456
var x = 56
var y = 759

test("Test brute force", () => {
  let curve = new Curve(a, b, p)
  let pt = new Point(x, y)
  let search = curve.multPoint(pt, n)
  var t0 = performance.now()

  let step = bruteForce(pt, search, a, b, p)

  var t1 = performance.now()
  console.log("Call to bruteForce took " + (t1 - t0) + " milliseconds.")
  expect(step).toEqual(n)
})

test("Test brute force big", () => {
  let curve = new Curve(a, b, p2)
  let pt = new Point(x, y)
  let search = curve.multPoint(pt, n2)
  var t0 = performance.now()

  let step = bruteForce(pt, search, a, b, p2)

  var t1 = performance.now()
  console.log("Call to bruteForce took " + (t1 - t0) + " milliseconds.")
  expect(step).toEqual(n2)
})

test("Test baby step giant step", () => {
  let curve = new Curve(a, b, p)
  let pt = new Point(x, y)
  let search = curve.multPoint(pt, n)
  let order = curve.groupOrder(pt)
  var t0 = performance.now()

  let step = bsgsElliptic(pt, search, a, b, p, order)

  var t1 = performance.now()
  console.log("Call to bsgsElliptic took " + (t1 - t0) + " milliseconds.")
  expect(step).toEqual(n)
})

test("Test baby step giant step big", () => {
  let curve = new Curve(a, b, p2)
  let pt = new Point(x, y)
  let search = curve.multPoint(pt, n2)
  let order = curve.groupOrder(pt)
  var t0 = performance.now()

  let step = bsgsElliptic(pt, search, a, b, p2, order)

  var t1 = performance.now()
  console.log("Call to bsgsElliptic took " + (t1 - t0) + " milliseconds.")
  expect(step).toEqual(n2)
})

// test("Test pollard's Rho", () => {
//   let curve = new Curve(a, b, p)
//   let pt = new Point(x, y)
//   let search = curve.multPoint(pt, n)
//   let order = curve.groupOrder(pt)
//   var t0 = performance.now()

//   let step = pollardRho(pt.y, search.y, p, order)

//   var t1 = performance.now()
//   console.log("Call to pollardRho took " + (t1 - t0) + " milliseconds.")
//   expect(step).toEqual(n)
// })
